# TRAIN Documentation

## Frequently asked questions

**Q: Do I need my own DIDs to configure a Trust Framework?**

**A:** Yes, having your own DIDs is necessary to configure a Trust Framework effectively. Please note that a service endpoint must be included in the DID document. See the Train Trust Framework Manager [installation documentation](https://gitlab.eclipse.org/eclipse/xfsc/train/tspa/-/tree/master/doc/install?ref_type=heads). External resources on DID creation can be found in [this project](https://github.com/eclipse-tractusx/SSI-agent-lib).

**Q: Is a fully qualified domain required to configure a Zone Manager?**

**A:** Yes, a fully qualified domain is required. The Zone Manager relies on the DNS system to establish trust. Without a fully qualified domain, DIDs and trust lists published through the Train Trust Framework Manager cannot be resolved via DNS.

**Q: Is DNSSEC really needed for Zone Manager Deployment?**

**A:** While the Zone Manager is designed to utilize the DNSSEC extension for enhanced trust verification, it is technically possible to operate without DNSSEC. However, this is not recommended due to potential security risks.

**Q: Does Train Trust Framework Manager support different trust lists formats?**

**A:** Yes, it currently supports XML and JSON formats. You can access schemas and templates for both formats in the [provided documentation](https://gitlab.eclipse.org/eclipse/xfsc/train/tspa/-/tree/master/doc/TrustList_DataModel_Design?ref_type=heads).

**Q: How does VC Signing work?**

**A:** The signature of verifiable credentials can be realized via the [TSA Signer](https://gitlab.eclipse.org/eclipse/xfsc/tsa/signer). Additionally, an internal signing mechanism has been implemented to offer more flexibility. Please refer to the [setup documentation](https://gitlab.eclipse.org/eclipse/xfsc/train/tspa/-/tree/master/doc/install?ref_type=heads).

**Q: Can TRAIN be only used in a federation context?**

**A:** No. TRAIN provides an additional layer of security for various ecosystems at different levels. For example, an organization can operate its own trust framework covering different subsidiaries, vendors, partners, or departments where this additional layer of trust is required or bring value.

**Q: Can the Train Trust Validator client libraries be integrated into software for trust validation purposes?**

**A:** Yes, the TTV offers client libraries in various programming languages to facilitate integration. For detailed information on languages and integration, please refer to the [TTV documentation](https://gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver/-/tree/main/docs/client_api?ref_type=heads).

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.eclipse.org/eclipse/xfsc/train/TRAIN-Documentation.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.eclipse.org/eclipse/xfsc/train/TRAIN-Documentation/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name

Choose a self-explaining name for your project.

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
